==========================
 Modifying the web design
==========================

Code and issues are at https://github.molgen.mpg.de/EditionOpenAccess/webdesign_platform

A tool called ``yarn`` is required for building the ``css``, ``js``, and ``html`` files. See :ref:`installation` for instructions.

Disseminating changes
=====================

Make sure after having made the changes that each active publication platform has its own colour scheme and that the final versions for the platform need their colours. The code is in ``src/styles/settings/color-theme.scss``. The variables are called::

  $color-main

  $color-secondary

See https://github.molgen.mpg.de/EditionOpenAccess/webdesign_platform/blob/master/README.md#available-colour-schemes-from-novamundo for the possible recommended combinations and which platform uses which theme.

It is probably a good idea to use some other, outrageous, theme for the local testing instance to keep you from pushing the wrong theme to the wrong platform.

Assuming that the user on local machine and staging machine are the same, the changes could be deployed like so::

  scp dist/app.js dist/app.css eoa2prod:eoapp_mprl/mprl/mprl/static
