==============
 Installation
==============

Cloning the repository
======================
Clone the repository with ``git``::

  git clone https://github.molgen.mpg.de/EditionOpenAccess/webdesign_platform.git

``yarn``
========

Install ``yarn``.

MacOS
-----
Install via `homebrew <https://brew.sh/>`_ ::

  brew install yarn

Linux
-----
Refer to https://classic.yarnpkg.com/en/docs/install

``nvm``
=======

There are issues with ``nvm`` versions. It works best if the ``nvm`` version is kept at version 10. For the Z Shell (``zsh``), there is a plugin for that: https://github.com/lukechilds/zsh-nvm. Clone it and source it with::

  source ~/src/zsh-nvm/zsh-nvm.plugin.zsh

And then use::

  nvm use 10

to set the version used.

Dependencies
============

Run::

  yarn

to download all dependencies and then::

  yarn build

in order to build ``app.css`` and ``app.js`` files to be stuck into the platform.

And for later, in one go::

  source ~/src/zsh-nvm/zsh-nvm.plugin.zsh && nvm use 10 && yarn build


Targets
=======

Several targets are available

General build command
---------------------
To build the files for production::

  yarn build

Build without minification
--------------------------
Like the general build command, but without minification of ``css`` and ``js`` files::

  yarn debug

Development server
------------------
Run the Webpack dev server::

  yarn dev

HTTP server
-----------
This runs an http-server on port 8082::

  yarn httpserver

And open http://127.0.0.1:8082.
