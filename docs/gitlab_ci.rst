===============
Using GitLab CI
===============

The repository is also configured to perform automated builds in GitLab. It is divided into two stages.

Pages
=====

``Pages`` builds the assets files (``.js``, ``.css``) as well as the ``html`` files from the ``pug`` templates. The result can be downloaded or viewed directly in GitLab.

Packages
========

``Packages`` also performs the complete build but discards the ``html`` files and keeps only those that are required for the publication platform: ``app.js``, ``app.css`` and the directory ``assets`` containing icons, fonts and Javascript libraries.

The main script is ``.gitlab_build_minimal.sh`` which takes three arguments as input: shorthand of the project, main colour, secondary colour.

In the current context, the file ``platforms.csv`` is used to keep the configurations of different platforms together, and they are built sequentially and stored as zipped files. They can be downloaded and distributed accordingly.
