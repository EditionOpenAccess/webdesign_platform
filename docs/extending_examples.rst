====================
 Extending Examples
====================

The examplary webdesign pages should be kept in the pug template language. At least, everything that can be expressed with the current ``css`` and ``js``.

To insert a new page, create a ``.pug`` file in ``src/pug`` and use the existing pages for inspiration. In order to have them rendered (at least when using the build system with ``yarn`` and ``webpack``), they need to be registered in the ``webpack`` configuration files, ``config/webpack.debug.conf.js``, ``config/webpack.dev.conf.js``, and ``config/webpack.prod.conf.js``. Then, use::

  yarn build

to render them.
