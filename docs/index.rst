.. EOA Publication Platform Webdesign documentation master file, created by
   sphinx-quickstart on Thu Apr  2 09:08:50 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EOA Publication Platform Webdesign's documentation!
==============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   installation
   modify_webdesign
   extending_examples
   gitlab_ci

This repository contains the resources for the web design of the EOA Publication Platform. For ``css``, ``scss`` is used.

The repository also features a complete set of example pages that are modeled after the publication examples found in https://github.molgen.mpg.de/EditionOpenAccess/eoa-publication-model. The pages are written in the template language `Pug <https://pugjs.org>`_. See :ref:`installation` for installation instructions.
