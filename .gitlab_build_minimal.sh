#!/bin/bash
if [ $# -lt 3 ]
  then
    echo "No arguments supplied. Three arguments required:\
 [name of archive file] [main color hex] [secondary color hex]"
  else
    echo ----------------
    echo Building for $1 platform
    echo Main colour is \#$2
    echo Secondary colour is \#$3
    sed -i.bak -E "s/^(\\\$color-main.{3})[a-f0-9]{3,6}/\1$2/g" src/styles/settings/color-theme.scss
    sed -i.bak -E "s/^(\\\$color-secondary.{3})[a-f0-9]{3,6}/\1$3/g" src/styles/settings/color-theme.scss
    npm run build
    rm dist/assets/images/{tux,1_shot_illustr,chinchar}.png
    rm dist/assets/images/*.jpg
    zip -r packages/$1.zip dist/a{p,s}*
    git reset --hard HEAD
    echo ----------------
fi
