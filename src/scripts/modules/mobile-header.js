/* eslint-disable */

export default () => {
  const nav = $('nav.nav');
  const navToggle = $('nav .nav-toggle');
  const navWrapper = $('nav .nav__wrapper');

  function mobileMenuOpened() {
    nav.toggleClass('open');
    navToggle.toggleClass('active');
    navWrapper.slideToggle();

    if ($('.nav').hasClass('open')) {
      $('body').css('overflow', 'hidden');
      $('.mobile-header__background').show();
    } else {
      $('body').css('overflow', 'auto');
      $('.mobile-header__background').hide();
    }
    return false;
  }

  $('.nav-toggle').on('click', mobileMenuOpened);
  $('.mobile-header__background').on('click', mobileMenuOpened);
};
