/* eslint-disable */

export default () => {
  $('.accordion').on('click', '.accordion__title', function acc(e) {
    e.preventDefault();
    if (
      !$('.publications-popup').is(e.target)
      && !$('.publications-popup-eye').is(e.target)
      && !$('.publications-popup-para').is(e.target)
      && !$('.publications-popup-sup').is(e.target)
    ) {
      $(this).closest('.accordion__item').toggleClass('active');
      $(this).next('.accordion__content').slideToggle(200);
    }
  });

  $('.accordion').on('click', '.accordion__showall', function accall(e) {
    e.preventDefault();
    if (!$(this).hasClass('show')) {
      $(this).addClass('show');
      $(this).text('Close All');
      $(this).siblings('.accordion__item').addClass('active');
      $(this).siblings('.accordion__item').children('.accordion__content').slideDown(200);
    } else {
      $(this).removeClass('show');
      $(this).text('Show All');
      $(this).siblings('.accordion__item').removeClass('active');
      $(this).siblings('.accordion__item').children('.accordion__content').slideUp(200);
    }
  });

  $('.accordion__pageup').on('click', (e) => {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
  });

  $('.publications-popup').on('click', function accpopup(e) {
    e.preventDefault();
    const text = $(this).data('content');
    const title = $(this).data('title');
    const elementClassic = `<div class="infobox active"><h4 class="infobox__title">${title}</h4><p class="infobox__description">${text}</p></div>`;
    if (!$(this).hasClass('active')) {
      $(this).after(elementClassic);
      $(this).addClass('active');
    } else {
      $(this).next().remove();
      $(this).removeClass('active');
    }
    const infobox = $(this).parent().find('.infobox');

    infobox.each((index, element) => {
      if (index === 0) {
        $(element).addClass('infobox--arrow-left');
        $(element).removeClass('infobox--arrow-top');
        $(element).css({ top: 0 });
      } else {
        $(element).addClass('infobox--arrow-top');
        $(element).removeClass('infobox--arrow-left');

        let topElem = 0;
        for (let i = 0; i < index; i += 1) {
          topElem = topElem + $(infobox[i]).outerHeight() + 20;
        }
        $(element).css({ top: `${topElem}px` });
      }
    });
  });

  $('button.publications-popup-eye').on('click', function popupeye(e) {
    e.preventDefault();
    const text = $(this).data('content');
    const title = $(this).data('title');
    const elementClassic = `<div class="infobox active"><h4 class="infobox__title">${title}</h4><p class="infobox__description">${text}</p></div>`;
    if (!$(this).hasClass('active')) {
      $(this).after(elementClassic);
      $(this).addClass('active');
    } else {
      $(this).next().remove();
      $(this).removeClass('active');
    }

    const infobox = $(this).parent().find('.infobox');

    infobox.each((index, element) => {
      if (index === 0) {
        $(element).addClass('infobox--arrow-left');
        $(element).removeClass('infobox--arrow-top');
        $(element).css({ top: 0 });
      } else {
        $(element).addClass('infobox--arrow-top');
        $(element).removeClass('infobox--arrow-left');

        let topElem = 0;
        for (let i = 0; i < index; i += 1) {
          topElem = topElem + $(infobox[i]).outerHeight() + 20;
        }
        $(element).css({ top: `${topElem}px` });
      }
    });
  });

  $('button.publications-popup-para').on('click', function popuppara(e) {
    e.preventDefault();
    const text = $(this).data('content');
    const title = $(this).data('title');
    const elementClassic = `<div class="infobox active"><h4 class="infobox__title">${title}</h4><p class="infobox__description">${text}</p></div>`;
    if (!$(this).hasClass('active')) {
      $(this).after(elementClassic);
      $(this).addClass('active');
    } else {
      $(this).next().remove();
      $(this).removeClass('active');
    }

    const infobox = $(this).parent().find('.infobox');

    infobox.each((index, element) => {
      if (index === 0) {
        $(element).addClass('infobox--arrow-left');
        $(element).removeClass('infobox--arrow-top');
        $(element).css({ top: 0 });
      } else {
        $(element).addClass('infobox--arrow-top');
        $(element).removeClass('infobox--arrow-left');

        let topElem = 0;
        for (let i = 0; i < index; i += 1) {
          topElem = topElem + $(infobox[i]).outerHeight() + 20;
        }
        $(element).css({ top: `${topElem}px` });
      }
    });
  });


  $('button.publications-popup-sup').on('click', function popupsup(e) {
    e.preventDefault();
    const text = $(this).data('content');
    const title = $(this).data('title');
    const elementClassic = `<div class="infobox active"><h4 class="infobox__title">${title}</h4><p class="infobox__description">${text}</p></div>`;
    if (!$(this).hasClass('active')) {
      $(this).after(elementClassic);
      $(this).addClass('active');
    } else {
      $(this).next().remove();
      $(this).removeClass('active');
    }

    const infobox = $(this).parent().find('.infobox');

    infobox.each((index, element) => {
      if (index === 0) {
        $(element).addClass('infobox--arrow-left');
        $(element).removeClass('infobox--arrow-top');
        $(element).css({ top: 0 });
      } else {
        $(element).addClass('infobox--arrow-top');
        $(element).removeClass('infobox--arrow-left');

        let topElem = 0;
        for (let i = 0; i < index; i += 1) {
          topElem = topElem + $(infobox[i]).outerHeight() + 20;
        }
        $(element).css({ top: `${topElem}px` });
      }
    });
  });

  $('.publications-popup-text').on('click', function popuptext(e) {
    e.preventDefault();
    const text = $(this).data('content');
    const title = $(this).data('title');
    const elementClassic = `<div class="infobox infobox-text active">
    <h4 class="infobox__title">${title}</h4><p class="infobox__description">${text}</p></div>`;
    if (!$(this).hasClass('active')) {
      $(this).after(elementClassic);
      $(this).addClass('active');
    } else {
      $(this).next().remove();
      $(this).removeClass('active');
    }

    const infobox = $(this).parent().find('.infobox');
    $(infobox).offset({ top: $(this).offset().top + 40 });
  });

  $(document).mouseup((e) => {
    const infobox = $('.infobox');
    if (
      !infobox.is(e.target)
      && !$('.publications-popup').is(e.target)
      && infobox.has(e.target).length === 0
      && !$('.publications-popup-eye').is(e.target)
      && !$('.publications-popup-sup').is(e.target)
      && !$('.publications-popup-para').is(e.target)
      && !$('.publications-popup-text').is(e.target)
      && !$('.publications-popup-text > *').is(e.target)
    ) {
      infobox.remove();
      $('.publications-popup').removeClass('active');
      $('.publications-popup-eye').removeClass('active');
      $('.publications-popup-sup').removeClass('active');
      $('.publications-popup-para').removeClass('active');
      $('.publications-popup-text').removeClass('active');
      e.stopPropagation();
    }
  });
  const urlHash = window.location.hash;
  $('.accordion__title').each(function showHashItem() {
    if ($(this).attr('href') === urlHash) {
      $(this).closest('.accordion__item').addClass('active');
      $(this).next('.accordion__content').css('display', 'block');
    }
  });
};
