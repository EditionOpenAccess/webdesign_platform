/* eslint-disable */

export default () => {
  function buttonOpen() {
    console.log($(this));
    if ($(this).hasClass('open')) {
      $(this).next('.more-block__drop-down').slideUp();
      $(this).removeClass('open');
    } else {
      $(this).next('.more-block__drop-down').slideDown();
      $(this).addClass('open');
    }
  }

  $('.more-block__btn').on('click', buttonOpen);
};
