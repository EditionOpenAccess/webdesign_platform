/* eslint-disable */

export default () => {
    $('.doi-menu__btn').on('click', function doiMenu() {

        $('.doi-menu__btn').toggleClass('open');

        $('.doi-menu').fadeToggle();
    });
};

function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

async function button_feedback(buttonid) {
    let fbk = document.getElementById(buttonid);
    fbk.textContent = 'Copied';
    await sleep(2000);
    fbk.textContent = 'Copy';
}

async function copy_text(buttonid, formatid) {
    let text_element = document.getElementById(formatid);

    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        // thanks to https://stackoverflow.com/a/57792449
        var range = document.createRange();
        range.selectNode(text_element);
        var selection = window.getSelection()
        selection.removeAllRanges();
        selection.addRange(range);

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copy command was ' + msg);
            button_feedback(buttonid);
        } catch (err) {
            console.log('Oops, unable to copy');
        }
        selection.removeAllRanges();
    }
    else
    {
        function listener(e) {
            e.clipboardData.setData("text/html", text_element.innerHTML);
            e.clipboardData.setData("text/plain", text_element.innerText);
            e.preventDefault();
        }
        try {
            document.addEventListener("copy", listener);
            document.execCommand("copy");
            document.removeEventListener("copy", listener);
            button_feedback(buttonid);
        } catch(err) {
            alert('Error in copying text: ', err);
        };
    };
};


function download(file, text) {
    //creating an invisible element
    var element = document.createElement('a');
    element.setAttribute('href',
                         'data:text/plain;charset=utf-8,'
                         + encodeURIComponent(text));
    element.setAttribute('download', file);

    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
};

$('#btn_dl_cit').on('click', function() {
    let mdci = document.querySelector('meta[name="DC.identifier"]');
    var isbn = mdci.getAttribute('content');
    var text = document.getElementById('citris_hidden').value;
    var filename = 'eoa_citation_export_' + isbn + '.ris';

    download(filename, text);
})

$('#btn_cp_doi').on('click', function(){
    copy_text('btn_cp_doi', 'doi')})

$('#btn_cp_cit').on('click', function(){
    copy_text('btn_cp_cit', 'dlcit')})
