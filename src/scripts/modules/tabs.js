/* eslint-disable */

export default () => {
  $('.tabs__items').on('click', '.tabs__item', (e) => {
    e.preventDefault();
  });

  $('.tabs__items').on('click', '.tabs__item:not(.active)', function tabs() {
    $(this)
      .addClass('active').siblings().removeClass('active')
      .closest('.tabs')
      .find('.tabs__content-item')
      .removeClass('active')
      .eq($(this).index())
      .addClass('active');
  });

  $('.tabs__items').on('click', '.tabs__item.active', function tabs() {
    $(this)
      .removeClass('active').siblings().removeClass('active')
      .closest('.tabs')
      .find('.tabs__content-item')
      .removeClass('active')
      .eq($(this).index())
      .removeClass('active');
  });

  $(window).on('load', () => {
    const tabsItem = $('.tabs__item');
    const tabsHead = $('.tabs__head');

    function notVisible() {
      if (window.innerWidth < 768) {
        tabsItem.each(function conditionVisible() {
          if ($(this).offset().left < 0 || tabsHead.innerWidth() <= $(this).offset().left + $(this).width() - 10) {
            $(this).addClass('tabs__item--not-visible');
          } else {
            $(this).removeClass('tabs__item--not-visible');
          }
        });
      } else {
        tabsItem.removeClass('tabs__item--not-visible');
      }
    }

    notVisible();

    $(window).on('resize', () => {
      notVisible();
    });

    tabsHead.on('scroll', () => {
      notVisible();
    });
  });
};
