/* eslint-disable */

export default () => {
  const blocks = $('.expand-block');

  blocks.each((id, elem) => {
    const block = $(elem);
    const more = block.find('.expand-block__more');

    more.on('click', (e) => {
      e.preventDefault();

      block.toggleClass('expand-block--opened');

      $(e.currentTarget).text(
        block.hasClass('expand-block--opened') ? 'Less' : 'More'
      );
    });
  });
};
