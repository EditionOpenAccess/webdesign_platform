/* eslint-disable */
import 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'magnific-popup';
import 'magnific-popup/dist/magnific-popup.css';
import polyfills from './polyfills/index';
import BlockExpand from './modules/BlockExpand';
import tabs from './modules/tabs';
import mobileHeader from './modules/mobile-header';
import acc from './modules/accordion';
import buttonDropdown from './modules/button-dropdown';
import objectFit from './polyfills/object-fit';
import chapterMenu from './modules/chapter-menu';
import doiMenu from './modules/doi-menu';

require('./thirdparty/mathjax-config.js');

(() => {
  polyfills();
  BlockExpand();
  tabs();
  mobileHeader();
  acc();
  buttonDropdown();
  objectFit();
  chapterMenu();
  doiMenu();

  if ($('.slick-slider').length) {
    $('.slick-slider').slick({
      dots: true,
      arrows: true,
      adaptiveHeight: true,
      swipeToSlide: true,
      centerMode: true,
      centerPadding: 0
    });
  }

  if ($('[data-lightbox]').length) {
    $('[data-lightbox]').magnificPopup({
      type: 'image',
      closeOnContentClick: true,
      fixedContentPos: true,
      image: {
        verticalFit: true
      },
      zoom: {
        enabled: true,
        duration: 300
      }
    });
  }
})();
