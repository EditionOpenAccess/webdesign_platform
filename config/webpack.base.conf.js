const StyleLintPlugin = require('stylelint-webpack-plugin')
const CopyPlugin = require("copy-webpack-plugin")

process.noDeprecation = true

module.exports = {
  module: {
    rules: [{
      test   : /\.pug$/,
      use    : [
        {
          loader: 'html-loader'
        },
        {
          loader: 'pug-html-loader',
          options: {
            pretty: true
          }
        }
      ]
    },
    {
      enforce : 'pre',
      test    : /\.js$/,
      exclude : [/node_modules/],
      loader  : 'eslint-loader'
    },
    {
      test    : /\.js?$/,
      exclude : [/node_modules/],
      loader  : 'babel-loader'
    },
    {
      test    : /\.(jpe?g|png|gif|svg)$/i,
      loader  : 'file-loader',
      options : {
        name       : '[name].[ext]',
        outputPath : 'assets/images/'
      }
    },
    {
      test    : /\.(woff(2)?|eot|ttf|otf)$/,
      loader  : 'file-loader',
      options : {
        name       : '[name].[ext]',
        outputPath : 'assets/fonts/'
      }
    }]
  },
  plugins: [
    new StyleLintPlugin(),
    new CopyPlugin([
        {
            from: 'src/assets/lib/jquery-3.5.1.min.js',
            to: 'assets/lib/',
        },
        {
            from: 'src/assets/lib/jquery.magnific-popup.min.js',
            to: 'assets/lib/',
        },
    ]),
  ]
}
