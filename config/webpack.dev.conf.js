const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const base = require('./webpack.base.conf');

module.exports = merge(base, {
  mode    : 'development',
  devtool : 'eval-source-map',
  output  : {
    filename   : 'app.js',
    publicPath : '/'
  },
  module: {
    rules: [{
      test : /(\.scss|\.css)$/,
      use  : ExtractTextPlugin.extract({
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader  : 'css-loader',
            options : {
              sourceMap: true
            }
          },
          {
            loader  : 'postcss-loader',
            options : {
              sourceMap : true,
              ident     : 'postcss',
              plugins   : () => [
                autoprefixer()
              ]
            }
          },
          {
            loader: 'resolve-url-loader'
          },
          {
            loader  : 'sass-loader',
            options : {
              sourceMap: true
            }
          }
        ]
      })
    }]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root    : path.join(__dirname, '..'),
      verbose : false
    }),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      disable: true
    }),
    new HtmlWebpackPlugin({
      template : './src/pug/index.pug',
      favicon  : 'favicon.png'
    }),
    new HtmlWebpackPlugin({
      template : './src/pug/toc.pug',
      favicon  : 'favicon.png'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter01.html',
      template: './src/pug/chapter01.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter02.html',
      template: './src/pug/chapter02.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter03.html',
      template: './src/pug/chapter03.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter04.html',
      template: './src/pug/chapter04.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter05.html',
      template: './src/pug/chapter05.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter06.html',
      template: './src/pug/chapter06.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter07.html',
      template: './src/pug/chapter07.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter08.html',
      template: './src/pug/chapter08.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter09.html',
      template: './src/pug/chapter09.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter10.html',
      template: './src/pug/chapter10.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter11.html',
      template: './src/pug/chapter11.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'chapter12.html',
      template: './src/pug/chapter12.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'facsimilepage.html',
      template: './src/pug/facsimilepage.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'facsimilepageenhanced.html',
      template: './src/pug/facsimilepageenhanced.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'landingpage.html',
      template: './src/pug/landingpage.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'listoffigures.html',
      template: './src/pug/listoffigures.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'series_single.html',
      template: './src/pug/series_single.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'editorial-board.html',
      template: './src/pug/editorial-board.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'authors-details.html',
      template: './src/pug/authors-details.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'series_multi.html',
      template: './src/pug/series_multi.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'publishing-conditions.html',
      template: './src/pug/publishing-conditions.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'contact.html',
      template: './src/pug/contact.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'publication-policy.html',
      template: './src/pug/publication-policy.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'authors.html',
      template: './src/pug/authors.pug'
    }),
    new HtmlWebpackPlugin({
      filename: 'imprint.html',
      template: './src/pug/imprint.pug'
    }),
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false)
    })
  ]
});
