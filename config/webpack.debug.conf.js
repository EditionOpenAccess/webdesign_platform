const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;

const base = require('./webpack.base.conf');

module.exports = merge(base, {
    mode   : 'none',
    output : {
        path     : path.resolve(__dirname, '../dist'),
        filename : 'app.js'
        //filename : '[contenthash].app.js'
    },
    optimization: {
        minimize: true
    },
    module: {
        rules: [{
            test : /(\.scss|\.css)$/,
            use  : ExtractTextPlugin.extract({
                use: [
                    {
                        loader  : 'css-loader',
                        options : {
                            minimize  : false,
                            sourceMap : true
                        }
                    },
                    {
                        loader: 'clean-css-loader',
                        options: {
                            format: 'beautify'
                        }
                    },
                    {
                        loader  : 'postcss-loader',
                        options : {
                            minimize: false,
                            sourceMap : true,
                            ident     : 'postcss',
                            plugins   : () => [
                                autoprefixer()
                            ]
                        }
                    },
                    {
                        loader: 'resolve-url-loader'
                    },
                    {
                        loader  : 'sass-loader',
                        options : {
                            sourceMap: true,
                        }
                    }
                ]
            })
        }]
    },
    plugins: [
        new CleanWebpackPlugin(['dist'], { verbose: false }),
        new ExtractTextPlugin({
            filename : 'app.css',
            // filename : '[chunkhash].app.css',
            disable  : false
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/index.html'),
            template : './src/pug/index.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/toc.html'),
            template : './src/pug/toc.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter01.html'),
            template : './src/pug/chapter01.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter02.html'),
            template : './src/pug/chapter02.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter03.html'),
            template : './src/pug/chapter03.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter04.html'),
            template : './src/pug/chapter04.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter05.html'),
            template : './src/pug/chapter05.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter06.html'),
            template : './src/pug/chapter06.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter07.html'),
            template : './src/pug/chapter07.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter08.html'),
            template : './src/pug/chapter08.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter09.html'),
            template : './src/pug/chapter09.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter10.html'),
            template : './src/pug/chapter10.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter11.html'),
            template : './src/pug/chapter11.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/chapter12.html'),
            template : './src/pug/chapter12.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/facsimilepage.html'),
            template : './src/pug/facsimilepage.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/facsimilepageenhanced.html'),
            template : './src/pug/facsimilepageenhanced.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/landingpage.html'),
            template : './src/pug/landingpage.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/listoffigures.html'),
            template : './src/pug/listoffigures.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/series_single.html'),
            template : './src/pug/series_single.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/editorial-board.html'),
            template : './src/pug/editorial-board.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/authors-details.html'),
            template : './src/pug/authors-details.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/series_multi.html'),
            template : './src/pug/series_multi.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/publishing-conditions.html'),
            template : './src/pug/publishing-conditions.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/contact.html'),
            template : './src/pug/contact.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/publication-policy.html'),
            template : './src/pug/publication-policy.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/authors.html'),
            template : './src/pug/authors.pug',
            favicon  : 'favicon.png',
        }),
        new HtmlWebpackPlugin({
            filename : path.resolve(__dirname, '../dist/imprint.html'),
            template : './src/pug/imprint.pug',
            favicon  : 'favicon.png',
        }),
        new ImageminPlugin({
            pngquant: {
                quality: '65-80'
            }
        }),
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(true)
        })
    ]
});
