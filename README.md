# webpack4-starter-kit

Webpack 4 with webpack-dev-server configuration.

## Build with Docker (recommended)

Requirements: docker-compose

	$ ./scripts/init.sh

## Requirements

- Node >= v6.x
- Yarn >= v1.1 | NPM >= v5.0

## Install dependencies

```
$ cd my-app-name
$ yarn
```

## Available tasks

```sh

# Runs development server (Webpack dev server)
$ yarn dev

# Build command
$ yarn build

# Runs http-server on port 8082
$ yarn httpserver

# build without minification
$ yarn debug
```

## Features

* [Webpack 4](https://github.com/webpack/webpack)
* [Webpack Dev Server](https://github.com/webpack/webpack-dev-server)
* [HMR](https://webpack.js.org/concepts/hot-module-replacement/)
* [Babel](https://babeljs.io/)
* [EsLint](https://eslint.org/docs/user-guide/getting-started)
* [StyleLint](https://github.com/stylelint/stylelint)
* [Sass](https://github.com/webpack-contrib/sass-loader)
* [Autoprefixer](https://github.com/postcss/autoprefixer)
* Webpack Plugins: Html, Copy, ExtractText, Define

# Anleitung Farben ändern:

Die Farben befinden sich, also Variabeln, in dieser Datei:
`src/styles/settings/color-theme.scss`

Um damit arebieten zu können sollte zunächst "Node" installiert werden:
https://nodejs.org/de/

Dann mittels Terminal oder command line zum Projektordner navigieren
(cd/...)

Im Projektordner dann folgenden Befehle ausführen

npm install

Dann können die Änderungen an den Farben vorgenommen werden.

Dann folgenden Befehle ausführen

npm run build

Dadurch werden die fertigen Dateien in /dist kompliert. Diese Dateien
werden dann auch für die Webseite benutzt benutzt.

# Available colour schemes (from Novamundo)
## Main colour green: #003b35
Secondary colours

Colour | Hex code
----|-----
red | #fa5c59
blue | #009ecf
yellow | #f7b513

## Main colour blue: #00074e
Secondary colours

Colour | Hex code
----|-----
turquoise | #66cc60
green | #55c247
yellow | #ffa224

NB the values you need to change are in `src/styles/settings/color-theme.scss`. The value `$color-main` is the main colour, while `$color-secondary` is the secondary colour.

### MPRL colours:
```
$color-main: #00074e;
$color-secondary: #55c247;
```
### EOS colours:
```
$color-main: #003b35;
$color-secondary: #fa5c59;
```
